package com.bilal.myapplication

import androidx.annotation.DrawableRes
import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import androidx.databinding.library.baseAdapters.BR

data class CurrencyItem(private val mCode: String) : BaseObservable() {
    @get:Bindable
    var code: String = ""
        set(value) {
            field = value
            notifyPropertyChanged(BR.code)
        }

    @get:Bindable
    var value: String = ""
        set(value) {
            field = value
            notifyPropertyChanged(BR.value)
        }

    @get:Bindable
    var name: String = ""
        set(value) {
            field = value
            notifyPropertyChanged(BR.name)
        }

    @get:Bindable
    var symbol: String = ""
        set(value) {
            field = value
            notifyPropertyChanged(BR.symbol)
        }

    @get:Bindable
    var flag: Int = -1
        set(value) {
            field = value
            notifyPropertyChanged(BR.flag)
        }

    constructor(mCode: String, mName: String, mSymbol: String, @DrawableRes mFlag: Int) : this(mCode) {
        this.flag = mFlag
        this.name = mName
        this.symbol = mSymbol
    }

    init {
        this.code = mCode
        this.value = ""
    }


}