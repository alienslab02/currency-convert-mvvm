package com.bilal.myapplication.api.currency

import com.bilal.myapplication.api.models.RetroApiResponse

interface CurrencyApi {
    suspend fun getLatestCurrencyRates(baseCurrency: String): RetroApiResponse<CurrencyResponse>
}