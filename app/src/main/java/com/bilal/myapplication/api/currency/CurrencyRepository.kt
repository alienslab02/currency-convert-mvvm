package com.bilal.myapplication.api.currency

import com.bilal.myapplication.api.BaseRepository
import com.bilal.myapplication.api.RetroNetwork
import com.bilal.myapplication.api.models.RetroApiResponse

object CurrencyRepository : BaseRepository(), CurrencyApi {
    const val URL_LATEST_RATES = "/latest"

    private val api = RetroNetwork.createService(CurrencyRetroService::class.java)

    override suspend fun getLatestCurrencyRates(baseCurrency: String): RetroApiResponse<CurrencyResponse> =
        executeSafely { api.getLatestRates(baseCurrency) }
}