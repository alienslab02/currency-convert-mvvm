package com.bilal.myapplication.api

import com.bilal.myapplication.api.interfaces.IRepository
import com.bilal.myapplication.api.models.ApiError
import com.bilal.myapplication.api.models.ApiResponse
import com.bilal.myapplication.api.models.RetroApiResponse
import org.json.JSONException
import org.json.JSONObject
import retrofit2.Response
import com.google.gson.stream.MalformedJsonException as MalformedJsonException1

const val MALFORMED_JSON_EXCEPTION_CODE = 0

abstract class BaseRepository : IRepository {

    override suspend fun <T : ApiResponse> executeSafely(call: suspend () -> Response<T>): RetroApiResponse<T> {
        try {
            val response: Response<T> = call.invoke()
            if (response.isSuccessful) {
                return RetroApiResponse.Success(response.code(), response.body()!!)
            }

            // Check if this is not a server side error (4** or 5**) then return error instead of success
            return RetroApiResponse.Error(detectError(response))

        } catch (exception: MalformedJsonException1) {
            return RetroApiResponse.Error(ApiError(MALFORMED_JSON_EXCEPTION_CODE, exception.localizedMessage))
        } catch (exception: Exception) {
            return RetroApiResponse.Error(ApiError(0, exception.localizedMessage))
        }
    }

    private fun <T : ApiResponse> detectError(response: Response<T>): ApiError {
        if (response.code() == 504) {
            // It is no internet connect error
            return ApiError(response.code(), "Server Error")
        }

        // hmm.. may be server side api error or network error
        val error: String? = response.errorBody()!!.string()
        return ApiError(response.code(), fetchErrorFromBody(error) ?: error ?: "Something went wrong")
    }
    
    private fun fetchErrorFromBody(response: String?): String? {
        response?.let {
            if (it.isNotBlank()) {
                try {
                    val obj = JSONObject(it)

                    if (obj.has("error")) {
                        return obj.getString("error") ?: ""
                    }
                } catch (e: JSONException) {
                    // return "Server sent some malformed data :o"
                }
            }
        }
        return null
    }
}