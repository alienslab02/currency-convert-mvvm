package com.bilal.myapplication.api.interfaces

import com.bilal.myapplication.api.models.ApiResponse
import com.bilal.myapplication.api.models.RetroApiResponse
import retrofit2.Response

internal interface IRepository {
    suspend fun <T : ApiResponse> executeSafely(call: suspend () -> Response<T>): RetroApiResponse<T>
}