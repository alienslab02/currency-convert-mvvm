package com.bilal.myapplication.api.currency

import com.bilal.myapplication.api.models.ApiResponse

data class CurrencyResponse(
    val base: String,
    val date: String,
    val rates: HashMap<String, Float>
) : ApiResponse()