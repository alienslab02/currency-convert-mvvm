package com.bilal.myapplication.api.interfaces

internal interface Network {
    fun initWith(baseUrl: String)
    fun <T> createService(serviceInterface: Class<T>): T
}