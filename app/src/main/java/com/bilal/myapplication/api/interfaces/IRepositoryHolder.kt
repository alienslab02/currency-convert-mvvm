package com.bilal.myapplication.api.interfaces

import com.bilal.myapplication.api.BaseRepository

interface IRepositoryHolder<T : BaseRepository> {
    val repository: T
}