package com.bilal.myapplication.api.currency

import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface CurrencyRetroService {

    @GET(CurrencyRepository.URL_LATEST_RATES)
    suspend fun getLatestRates(@Query("base") base: String): Response<CurrencyResponse>

}