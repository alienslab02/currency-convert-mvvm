package com.bilal.myapplication.api.models

data class ApiError(
    var statusCode: Int,
    var message: String = ""
)