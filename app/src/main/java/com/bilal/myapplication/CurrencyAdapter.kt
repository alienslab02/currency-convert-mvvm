package com.bilal.myapplication

import androidx.databinding.ViewDataBinding
import androidx.databinding.library.baseAdapters.BR
import com.bilal.myapplication.core.BaseBindingRecyclerAdapter
import com.bilal.myapplication.core.BindingViewHolder

class CurrencyAdapter(currencies: MutableList<CurrencyItem>) :
    BaseBindingRecyclerAdapter<CurrencyItem, CurrencyAdapter.CurrencyViewHolder>(currencies) {

    override fun onCreateViewHolder(binding: ViewDataBinding): CurrencyViewHolder {
        return CurrencyViewHolder(binding)
    }

    override fun getLayoutIdForViewType(viewType: Int): Int = R.layout.item_currency

    inner class CurrencyViewHolder(binding: ViewDataBinding) :
        BindingViewHolder<CurrencyItem>(binding) {
        override val bindingVariable: Int = BR.currency
    }
}