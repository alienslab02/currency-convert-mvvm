package com.bilal.myapplication

import androidx.lifecycle.MutableLiveData
import com.bilal.myapplication.api.currency.CurrencyResponse
import com.bilal.myapplication.core.IBase

interface IMain {

    interface View : IBase.View<ViewModel>

    interface ViewModel : IBase.ViewModel<State> {
        val BASE_CURRENCY: String
            get() = "EUR"
        var currencyApiResponse: MutableLiveData<CurrencyResponse?>
        var currencies: MutableList<CurrencyItem>
        fun selectCurrency(code: String)
        fun handlePressOnCurrencyItem(position: Int)
    }

    interface State : IBase.State
}