package com.bilal.myapplication.core

import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import com.bilal.myapplication.BR


abstract class BaseState : BaseObservable(), IBase.State {

    @get:Bindable
    override var loading: Boolean = false
        set(value) {
            field = value
            notifyPropertyChanged(BR.loading)
        }

    @get:Bindable
    override var toast: String = ""
        set(value) {
            field = value
            notifyPropertyChanged(BR.toast)
        }

    override fun reset() {
        loading = false
        toast = ""
    }

    override fun destroy() {

    }

    override fun init() {

    }

    override fun resume() {

    }

    override fun pause() {

    }

}