package com.bilal.myapplication.core

import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView

abstract class BindingViewHolder<T : Any>(val binding: ViewDataBinding) :
    RecyclerView.ViewHolder(binding.root) {

    abstract val bindingVariable: Int

    open fun bind(obj: T) {
        binding.setVariable(bindingVariable, obj)
        binding.executePendingBindings()
    }
}