package com.bilal.myapplication.core

import android.content.Context

interface IBase {
    interface View<V : ViewModel<*>> {
        val viewModel: V
        fun showLoader(isVisible: Boolean)
        fun showToast(msg: String)
        fun showInternetSnack(isVisible: Boolean)
        fun isPermissionGranted(permission: String): Boolean
        fun requestPermissions()
        fun getString(resourceKey: String): String
    }

    interface ViewModel<S : State> : ILifecycle {
        val state: S
        val context: Context
    }

    interface State {
        var toast: String
        var loading: Boolean
        fun reset()
        fun destroy()
        fun init()
        fun resume()
        fun pause()
    }
}
