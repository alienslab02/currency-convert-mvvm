package com.bilal.myapplication.core

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Job

internal interface CoroutineViewModel {
    val viewModelJob: Job
    val viewModelScope: CoroutineScope
    fun cancelAllJobs()
    fun launch(block: suspend () -> Unit)
}