package com.bilal.myapplication.core

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView


abstract class BaseBindingRecyclerAdapter<T: Any, VH: BindingViewHolder<T>>(private val list: MutableList<T>) :
    RecyclerView.Adapter<VH>() {

    var onItemClickListener: OnItemClickListener? = null

    protected abstract fun onCreateViewHolder(binding: ViewDataBinding): VH

    protected abstract fun getLayoutIdForViewType(viewType: Int): Int

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VH {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding =
            DataBindingUtil.inflate<ViewDataBinding>(layoutInflater, getLayoutIdForViewType(viewType), parent, false)
        return onCreateViewHolder(binding)
    }

    override fun onBindViewHolder(holder: VH, position: Int) {
        val obj = getDataForPosition(position)
        holder.bind(obj)

        // Set click listeners
        onItemClickListener?.let {
            holder.binding.root.setOnClickListener {
                onItemClickListener?.onItemClick(it, holder.adapterPosition)
            }
        }
    }

    open fun getDataForPosition(position: Int): T {
        return list[position]
    }

    override fun getItemCount(): Int {
        return list.size
    }

    open fun setList(list: List<T>) {
        this.list.clear()
        this.list.addAll(list)
        notifyDataSetChanged()
    }

    interface OnItemClickListener {
        fun onItemClick(view: View, pos: Int)
    }

}
