package com.bilal.myapplication

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.databinding.Observable
import androidx.databinding.library.baseAdapters.BR
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bilal.myapplication.core.BaseBindingRecyclerAdapter
import com.bilal.myapplication.core.BaseState
import com.bilal.myapplication.databinding.ActivityMainBinding

@Suppress("DEPRECATION")
class MainActivity : AppCompatActivity() {

    val viewModel: MainViewModel
        get() = ViewModelProviders.of(this).get(MainViewModel::class.java)

    lateinit var currencyAdapter: CurrencyAdapter
    lateinit var currencyListView: RecyclerView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        performDataBinding()
        registerObservers()

        // Setup RecyclerView with Adapter
        currencyListView = findViewById<RecyclerView>(R.id.currencyList)
        currencyAdapter = CurrencyAdapter(viewModel.currencies)
        currencyAdapter.onItemClickListener =
            object : BaseBindingRecyclerAdapter.OnItemClickListener {
                override fun onItemClick(view: View, pos: Int) {
                    viewModel.handlePressOnCurrencyItem(pos)
                    currencyAdapter.notifyDataSetChanged()
                    currencyListView.smoothScrollToPosition(0)

                }
            }
        currencyListView.apply {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(this@MainActivity)
            adapter = currencyAdapter
        }
    }

    override fun onDestroy() {
        unregisterObservers()
        super.onDestroy()
    }

    private fun performDataBinding() {
        val viewDataBinding =
            DataBindingUtil.setContentView<ActivityMainBinding>(this, R.layout.activity_main)
        viewDataBinding.setVariable(BR.viewModel, viewModel)
        viewDataBinding.executePendingBindings()
    }

    private fun registerObservers() {
        viewModel.currencyApiResponse.observe(this, Observer {
            currencyAdapter.notifyDataSetChanged()
        })
        viewModel.registerLifecycleOwner(this)
        (viewModel.state as BaseState).addOnPropertyChangedCallback(stateObserver)
    }

    private fun unregisterObservers() {
        viewModel.unregisterLifecycleOwner(this)
        (viewModel.state as BaseState).removeOnPropertyChangedCallback(stateObserver)
    }

    private val stateObserver = object : Observable.OnPropertyChangedCallback() {
        override fun onPropertyChanged(sender: Observable?, propertyId: Int) {
            if (propertyId == BR.toast && viewModel.state.toast.isNotBlank()) {
                showToast(viewModel.state.toast)
            }
        }
    }

    fun showToast(msg: String) {
        if ("" != msg.trim { it <= ' ' }) {
            Toast.makeText(this, msg, Toast.LENGTH_LONG).show()
        }
    }


}
