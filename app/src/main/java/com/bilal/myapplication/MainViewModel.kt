package com.bilal.myapplication

import android.app.Application
import android.os.Handler
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import androidx.databinding.Observable
import androidx.databinding.library.baseAdapters.BR
import androidx.lifecycle.MutableLiveData
import com.bilal.myapplication.api.currency.CurrencyRepository
import com.bilal.myapplication.api.currency.CurrencyResponse
import com.bilal.myapplication.api.interfaces.IRepositoryHolder
import com.bilal.myapplication.api.models.RetroApiResponse
import com.bilal.myapplication.core.BaseViewModel

class MainViewModel(application: Application) : BaseViewModel<IMain.State>(application),
    IMain.ViewModel, IRepositoryHolder<CurrencyRepository> {
    override val repository: CurrencyRepository
        get() = CurrencyRepository
    override val state: MainState = MainState()

    override var currencyApiResponse: MutableLiveData<CurrencyResponse?> = MutableLiveData()
    override var currencies: MutableList<CurrencyItem> = arrayListOf()

    var selectedCurrency: CurrencyItem? = null

    override fun onCreate() {
        super.onCreate()
        handler.post(looper)
    }

    override fun handlePressOnCurrencyItem(position: Int) {
        selectCurrency(currencies[position].code)
    }

    override fun selectCurrency(code: String) {
        currencies.apply {
            val index = indexOfFirst { currency -> currency.code == code }
            if (index >= 0) add(0, removeAt(index))
        }
        selectCurrency(currencies[0])
    }

    private fun selectCurrency(currency: CurrencyItem) {
        selectedCurrency?.removeOnPropertyChangedCallback(currencyObserver)
        selectedCurrency = currency
        selectedCurrency?.addOnPropertyChangedCallback(currencyObserver)
    }

    private val currencyObserver = object : Observable.OnPropertyChangedCallback() {
        override fun onPropertyChanged(sender: Observable?, propertyId: Int) {
            when (propertyId) {
                BR.value -> selectedCurrency?.value?.let {
                    val v = if (it.isBlank()) 0f else {
                        try {
                            it.toFloat()
                        } catch (e: NumberFormatException) {
                            0f
                        }
                    }
                    updateCurrencyValues(v)
                }
            }
        }
    }

    /**
     * Function gets called when User has updated value in the selected currency field. It is the time to
     * calculate the value for other currencies based on the conversion rates
     */
    private fun updateCurrencyValues(value: Float) {
        selectedCurrency?.let {
            currencies.forEach { currency ->
                if (it.code != currency.code) {
                    currency.value = convert(value, it, currency)
                }
            }
        }
    }

    private fun convert(value: Float, from: CurrencyItem, to: CurrencyItem): String {
        if (value == 0f) return ""
        val rates = currencyApiResponse.value!!.rates
        return if (from.code == BASE_CURRENCY) {
            (value * rates[to.code]!!).toString()
        } else {
            ((value / rates[from.code]!!) * rates[to.code]!!).toString()
        }
    }

    private fun refreshCurrencyRates(data: CurrencyResponse) {
        if (currencies.size > 0) return
        // First time load
        currencies.clear()

        // Add rest of the currencies
        data.rates.keys.forEach { code ->
            CurrencyUtils.getCurrencyItemByCode(code)?.let { c -> currencies.add(c) }
        }
        selectCurrency(BASE_CURRENCY)


    }

    private fun requestRates() {
        launch {
            when (val response = repository.getLatestCurrencyRates(BASE_CURRENCY)) {
                is RetroApiResponse.Success -> {
                    val cr = response.data
                    cr.rates[BASE_CURRENCY] = 1f
                    refreshCurrencyRates(cr)
                    currencyApiResponse.value = cr
                }
                is RetroApiResponse.Error -> {
                    state.toast = response.error.message
                }
            }

            // Start the infinite loop
            handler.postDelayed(looper, 1000)
        }
    }

    private val handler = Handler()
    private val looper = Runnable { requestRates() }

    override fun onDestroy() {
        super.onDestroy()
        selectedCurrency?.removeOnPropertyChangedCallback(currencyObserver)
        handler.removeCallbacks(looper)
    }

    companion object {
        @BindingAdapter("src")
        @JvmStatic
        fun setImageResId(view: ImageView, resId: Int) {
            view.setImageResource(resId)
        }
    }

}